var express = require("express")
var path = require("path")
var bodyParser = require("body-parser")
var app = express()
var { check, validationResult } = require("express-validator")

// pricing object used for total price calculation
const pricing = {
    tax: {
        ON: 0.13,
        MN: 0.13,
        BC: 0.12,
        QC: 0.149,
        Al: 0.05,
        NB: 0.15,
        NT: 0.05,
        NS: 0.15,
        NV: 0.05,
        PEI: 0.15,
        SK: 0.11,
        YK: 0.05
    },
    item: {
        cappuccino: 4.30,
        latte: 4.35,
        macchiato: 4.50
    },
    shipping: {
        1: 10,
        2: 7,
        3: 4,
        4: 1.30
    },
}

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(__dirname + "/public"))

app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

app.get("/", function(req, res) {
    res.render("index")
})
app.post("/", [
            check("name", "Please enter the name").not().isEmpty(),
            check("product", "Please select a product").not().isEmpty(),
            check("email", "Please enter the email").not().isEmpty(),
            check("phone", "Please enter the phone").not().isEmpty(),
            check("city", "Please select your city").not().isEmpty(),
            check("province", "Please select your province").not().isEmpty(),
            check("shipping", "Please select your shipping type").not().isEmpty(),
            check("postCode").custom(value => {
                //Checking postal code conformity
                const reg = /^[a-zA-Z0-9]{3}\s?[a-zA-Z0-9]{3}$/
                if (!reg.test(value)) {
                    throw new Error("Please enter valid postal code")
                } else {
                    return true
                }
            }),
            check("quantity").custom(value => {
                console.log(value);
                //Quantity should be more than 1 and only numbers between 1 - 99
                const reg = /^[1-9]{1,2}$/
                if (!reg.test(value)) {
                    throw new Error("Please enter numbers for quantity")
                } else {
                    return true
                }
            })
        ],
        function(req, res) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.render('index', errors);
                console.log(errors);
            } else {
                const order = req.body
                const { item, shipping, tax } = pricing
                //Calculating total price
                const totalPrice = ((item[order.product] * tax[order.province] + item[order.product]) * parseInt(order.quantity) + shipping[order.shipping]).toFixed(2)
                    //Creating receipt object for passing to view
                const receipt = {
                    name: order.name,
                    item: order.product.toUpperCase(),
                    address: order.address + "/ " + order.city.toUpperCase() + "/ " + order.province,
                    totalPrice: totalPrice,
                    itemPrice: item[order.product],
                    tax: tax[order.province] * 100 + "%",
                    shippingFee: shipping[order.shipping],
                    shipping: order.shipping,
                    quantity: order.quantity
                }
                res.render('index', { receiptData: receipt });
            }
        })
    //All unsupported pages will go to 404
app.get("/*", function(req, res) {
    res.render("404")
})
app.listen(8080)
console.log("app started");